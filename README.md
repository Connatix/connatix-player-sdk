# connatix-player-sdk

Special repository for Swift Package Manager integration.

iOS native SDK for the Connatix player. Not fully compatible with Objective-C.

For that use case, please use `connatix-player-sdk-objc` instead.

Please consult the official Connatix support documentation for further information.
