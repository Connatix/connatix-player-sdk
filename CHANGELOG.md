# 5.1.7

* Internal improvements

# 5.1.6

* Updated OM SDK from version `1.5.1` to `1.5.2`
* Fixed the bitcode related issue caused by OM SDK during uploads to TestFlight on Xcode 16

# 5.1.3

* Updated OM SDK from version `1.4.13` to `1.5.1`

# 5.1.1

* Internal improvements

# 5.1.0

* Added support for separate domains between content and ads via the `useContentOnlyDomain` property of the `AppSettings` struct

# 5.0.1

* Added public `delegate` property on `ElementsPlayer` and `PlayspacePlayer` classes

# 5.0.0

* For more detailed code documentation related to the changes in this major version, please consult the official Connatix support documentation
* The `ElementsPlayer` and `PlayspacePlayer` external classes are now wrappers instead of direct views
* `ConnatixPlayer` base class no longer exists
* Separated `PlayerSettings` model into `ElementsSettings` and `PlayspaceSettings`
* Separated `Customization` model into `ElementsCustomization` and `PlayspaceCustomization`
* Added ability to enable/disable logging in the SDK via the `shouldLog` boolean
* The configuration object is now used at initialization instead of `setConfig`. The `start` method now renders the player instead of the `setConfig` method
* Objective-C variant of the SDK has been discontinued. For full Objective-C support, please use the previous major SDK version
* Existing methods have been modified to support the additional `shouldTrackEvent` boolean
* Internal improvements
* The `BaseAPI` protocol has the following additional methods/properties:
    - `playerWebView`
    - `onPlayerEvent`
    - `shouldLog`
    - `start()` (instead of `setConfig()`)
    - `stop()` (renamed from `stopPlayer()`)
    - `getSessionDataConfig()`
    - `updateConnectionType()`
    - `getConnectionType()`
    - `isViewable`
    - `getMacroValue`
    - `setLineItemMetadata`
* The `ElementsAPI` protocol has the following additional methods:
    - `getPlayerType()`
    - `getPlaylistId()`
    - `getPlaylistDetails()`
    - `setPlaylistDetails()`
    - `getVideoDetails()`
    - `setVideoPosition()`
    - `getApiConfig()`
    - `trackCustomClick()`
    - `loadExternalMedia()`
    - `setSize()`
    - `getSize()`
    - `toggleFullscreenButton()`
    - `prevVideo()`
    - `nextVideo()`
    - `disableAdsInBackground()`
    - `enableAdsInBackground()`
    - `updateJsQueryTargeting()`
    - `removeJsQueryTargeting()`
    - `updateDynamicCuePoints()`
* The `PlayspaceAPI` protocol has the following additional method:
    - `getApiConfig()`
* The `listenFor` method from `EventsAPI` no longer accepts a boolean as parameter
* The `receivedHTMLString` delegate method has been added to `ConnatixPlayerDelegate`
* Added the following events in `EventType`:
    - `adOpportunity`
    - `adCycleCompleted`
    - `impressionRevenue`
    - `bid`
    - `prefetchStart`
    - `omidAdSessionStart`
    - `omidAdSessionFinish`
    - `videoCompleted1`
    - `sizeChanged`
    - `collapsed`
    - `closeButtonClicked`
    - `stayNextButtonsTriggered`
    - `apiButtonClicked`
    - `ctaButtonClicked`
    - `stopped`
    - `buffering`
    - `preroll`
    - `postroll`
    - `hidden`
    - `galleryCardClicked`
* Removed the following event from `EventType`:
    - `setupError`
* Changed the following list of external models to accommodate the new APIs:
    - `Advertising`
    - `AppSettings`
    - `CloseButtonMode`
    - `ConnectionType`
    - `ElementsConfig`
    - `EventType`
    - `LineItem`
    - `OutstreamSettings`
    - `PlayerType`
    - `PlayspaceConfig`
* Added the following list of external models to accommodate the new APIs:
    - `AdBreak`
    - `AdBreakTargeting`
    - `AdBreakType`
    - `AdPodTargeting`
    - `AdRollType`
    - `AdSlotTargetingType`
    - `AdViewabilityPolicy`
    - `ConnatixSDKInfo`
    - `CtaButton`
    - `CuePointsSettings`
    - `ElementsCustomization`
    - `ElementsSettings`
    - `ExternalMedia`
    - `ExternalSource`
    - `ExternalVideoQuality`
    - `Gallery`
    - `GalleryOrientation`
    - `GalleryStyle`
    - `LineItemAdBreakSettings`
    - `LineItemMetadata`
    - `MediaClickType`
    - `NonlinearAdType`
    - `NonlinearPlayerSettings`
    - `OutstreamInitialRendering`
    - `PlayerSize`
    - `PlaylistDetails`
    - `PlaylistItem`
    - `PlaylistMetadata`
    - `PlayspaceCustomization`
    - `ResetCapType`
    - `SessionDataConfig`
    - `SkipAdSettings`
    - `SkipMode`
    - `Skippability`
    - `Source`
    - `TargetingType`
    - `VideoChapter`
    - `VideoSourceMetadata`

# 4.5.2

* Updated OM SDK from version `1.4.13` to `1.5.2` [Cherry Pick from SDK `5.1.6`]
* Fixed the bitcode related issue caused by OM SDK during uploads to TestFlight on Xcode 16

# 4.4.10

* Internal improvements

# 4.4.9

* Added Privacy Manifest for the Connatix SDK and its peer dependencies
* Signed the Connatix binary

# 4.4.8

* Internal improvements
* Built with the latest Xcode as of 18.03.2024

# 4.4.7

* Collect GPP SID as per IAB specification: https://github.com/InteractiveAdvertisingBureau/Global-Privacy-Platform/blob/main/Core/CMP%20API%20Specification.md#in-app-details
* Changed the IMA SDK dependency resolution to get the latest compatible version up until the next major instead of the next minor version

# 4.4.6

* Collect GPP String as per IAB specification: https://github.com/InteractiveAdvertisingBureau/Global-Privacy-Platform/blob/main/Core/CMP%20API%20Specification.md#in-app-details

# 4.4.4

* IMA SDK improvements on how app background/foreground flows are handled

# 4.4.3

* Built with the latest Xcode as of 30.01.2024

# 4.4.2

* A `sessionInView` event has been added to `EventsAPI` and it is now sent via `ConnatixPlayerDelegate` once per session after the content has played for 2 seconds [Cherry Pick from SDK `4.3.17`]

# 4.4.1

* IMA SDK improvements
* Updated OM SDK from version `1.4.2` to `1.4.9` [Cherry Pick from SDK `4.3.16`]

# 4.4.0

* Integrated Native Google IMA SDK, being distributed as an additional dependency of the Connatix SDK
* Monetization improvements

# 4.3.23

* Updated OM SDK to version `1.5.2` [Cherry Pick from SDK `5.1.6`]
* Fixed the bitcode related issue caused by OM SDK during uploads to TestFlight on Xcode 16

# 4.3.20

* Internal improvements

# 4.3.19

* Added Privacy Manifest for the Connatix SDK and its peer dependencies [Cherry Pick from SDK `4.4.9`]
* Signed the Connatix binary

# 4.3.18

* Built the latest Xcode as of 11.02.2024

# 4.3.17

* A `sessionInView` event has been added to `EventsAPI` and it is now sent via `ConnatixPlayerDelegate` once per session after the content has played for 2 seconds

# 4.3.16

* Updated OM SDK from version `1.4.2` to `1.4.9`

# 4.3.14

* A `ready` event has been added to `EventsAPI` and it is now sent via `ConnatixPlayerDelegate` when the player is rendered

# 4.3.13

* OM SDK improvements

# 4.3.12

* OM SDK improvements

# 4.3.11

* Fix for the all parameters constructor of `ElementsConfig`

# 4.3.10

* Swift Package Manager support
* Internal fix on how the `enableNewUI` property from the `Customization` struct is read when decoding

# 4.3.9

* Changed the `_appSettings` public property on `ElementsConfig` and `PlayspaceConfig` from `let` to `var`

# 4.3.8

* Changed `reactNativeSdkVersion` and `flutterSdkVersion` public properties on `AppSettings` from `let` to `var`

# 4.3.6

* Added optional `enableNewUI` public property on the `Customization` struct
* Added optional `reactNativeSdkVersion` and `flutterSdkVersion` public properties on the `AppSettings` struct. Do not use those properties directly as they are meant for the Connatix React-Native and Flutter bridge SDKs

# 4.3.5

* Fixed `sdkVersion` returning wrong value

# 4.3.4

* Built with newest Xcode as of 18.04.2023

# 4.3.3

* Internal improvements on how `EventsAPI` works

# 4.3.2

* Internal improvements on how `EventsAPI` works

# 4.3.1

* Fixed unreliable behavior of `remove` event method from the `EventsAPI`
* Internal improvements on how `EventsAPI` works

# 4.3.0

* IAB Certification for OM SDK: https://iabtechlab.com/compliance-programs/compliant-companies/
* Updated OM SDK from version `1.3.25` to `1.4.2`
* OM SDK improvements
* Internal improvements on how `EventsAPI` works

# 4.2.5

* Monetization improvements

# 4.2.4

* Internal improvements
* Send connection type (`3G, 4G, Wifi` etc) at player initialization to improve monetization
* Update connection type if it changes during the player session to improve monetization

# 4.2.3

* Internal improvements

# 4.2.2

* Added error codes for `BaseAPIError`, `ElementsAPIError` and `PlayspaceAPIError` via the public `code` property

# 4.2.0

* Added `mediaIdList` and `useMediaIdListAsPlaylist` public properties on `ElementsConfig`
* Added support for `getVideoIndex` and `setVideoIndex` API methods

# 4.1.0

* Internal improvements

# 4.0.11

* Internal improvements

# 4.0.10

* Added the public `onPlayerEvent` closure on `ConnatixPlayer` as an alternative to using `ConnatixPlayerDelegate`

# 4.0.9

* Fixed `sdkVersion` returning wrong value

# 4.0.8

* Built with Xcode `13.2.1` instead of `13.4` to fix Apple known compatibility issues

# 4.0.7

* OM SDK bug fixes and improvements
* Added new public `sdkVersion` static property on `ConnatixPlayer` which returns the current SDK version

# 4.0.6

* OM SDK improvements

# 4.0.5

* OM SDK improvements

# 4.0.4

* Added `getQualityPlayspaceImageStoryError` case in the `BaseAPIError` enum when calling `getQuality` on Playspace players with images instead of videos

# 4.0.3

* Internal improvements
* Shifted setting `translatesAutoresizingMaskIntoConstraints` to host application
* Fixed some cases where OM SDK would spam JavaScript error messages

# 4.0.2

* Made the `delegate` property of `ConnatixPlayer` public

# 4.0.1

* Internal improvements

# 4.0.0

* Latest major version
