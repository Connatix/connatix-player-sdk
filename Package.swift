// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

// Copyright (c) 2024 Connatix
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import PackageDescription

let version = "5.1.7"
let checksum = "59d522a48c55a21a5cdf2b869574c4b362beefcecebb5b805b1e584ead65772c"

let package = Package(
    name: "connatix-player-sdk",
    platforms: [.iOS(.v12)],
    products: [
        .library(
            name: "ConnatixPlayerSDK",
            targets: ["ConnatixPlayerSDKTarget"]
        ),
    ],
    dependencies: [
        .package(
            name: "ConnatixAppMeasurement",
            url: "https://gitlab.com/Connatix/connatix-app-measurement.git",
            .exact("1.5.3")
        ),
        .package(
            name: "GoogleInteractiveMediaAds",
            url: "https://github.com/googleads/swift-package-manager-google-interactive-media-ads-ios.git",
            .upToNextMajor(from:"3.22.0")
        )
    ],
    targets: [
        .target(
            name: "ConnatixPlayerSDKTarget",
            dependencies: [
                .target(
                    name: "ConnatixPlayerSDK"
                ),
                .product(
                    name: "ConnatixAppMeasurement",
                    package: "ConnatixAppMeasurement"
                ),
                .product(
                    name: "GoogleInteractiveMediaAds",
                    package: "GoogleInteractiveMediaAds"
                )
            ],
            path: "ConnatixPlayerSDKTarget"
        ),
        .binaryTarget(
            name: "ConnatixPlayerSDK",
            url: "https://gitlab.com/Connatix/connatix-player-sdk/-/raw/\(version)/ConnatixPlayerSDK.zip",
            checksum: checksum
        )
    ]
)
